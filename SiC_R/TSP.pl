﻿% -----------------------------------------------------------------------
% Trabalho prático: factos de cidades com localizacao baseada em
% latitude e longitude e predicado auxiliar para calcular a distancia
% entre quaisquer duas destas cidades.
% ------------------------------------------------------------------------
:-dynamic dist/3.
:-dynamic city/3.

%city(name,latitude,longitude)
city(brussels,50.8462807,4.3547273).
city(porto,41.1621376,-8.6569732).
city(tirana,41.33165,19.8318).
city(andorra,42.5075025,1.5218033).
city(vienna,48.2092062,16.3727778).
city(minsk,53.905117,27.5611845).
city(sarajevo,43.85643,18.41342).
city(sofia,42.6976246,23.3222924).
city(zagreb,45.8150053,15.9785014).
city(nicosia,35.167604,33.373621).
city(prague,50.0878114,14.4204598).
city(copenhagen,55.6762944,12.5681157).
city(london,51.5001524,-0.1262362).
city(tallinn,59.4388619,24.7544715).
city(helsinki,60.1698791,24.9384078).
city(paris,48.8566667,2.3509871).
city(marseille,43.296386,5.369954).
city(tbilisi,41.709981,44.792998).
city(berlin,52.5234051,13.4113999).
city(athens,37.97918,23.716647).
city(budapest,47.4984056,19.0407578).
city(reykjavik,64.135338,-21.89521).
city(dublin,53.344104,-6.2674937).
city(rome,41.8954656,12.4823243).
city(pristina,42.672421,21.164539).
city(riga,56.9465346,24.1048525).
city(vaduz,47.1410409,9.5214458).
city(vilnius,54.6893865,25.2800243).
city(luxembourg,49.815273,6.129583).
city(skopje,42.003812,21.452246).
city(valletta,35.904171,14.518907).
city(chisinau,47.026859,28.841551).
city(monaco,43.750298,7.412841).
city(podgorica,42.442575,19.268646).
city(amsterdam,52.3738007,4.8909347).
city(belfast,54.5972686,-5.9301088).
city(oslo,59.9138204,10.7387413).
city(warsaw,52.2296756,21.0122287).
city(lisbon,38.7071631,-9.135517).
city(bucharest,44.430481,26.12298).
city(moscow,55.755786,37.617633).
city(san_marino,43.94236,12.457777).
city(edinburgh,55.9501755,-3.1875359).
city(belgrade,44.802416,20.465601).
city(bratislava,48.1483765,17.1073105).
city(ljubljana,46.0514263,14.5059655).
city(madrid,40.4166909,-3.7003454).
city(stockholm,59.3327881,18.0644881).
city(bern,46.9479986,7.4481481).
city(kiev,50.440951,30.5271814).
city(cardiff,51.4813069,-3.1804979).

%Prevents duplicate insertions of Cities. Use THIS instead of "assert"
assertCity(Cidade,Latitude,Longitude):-
    \+city(Cidade,_,_),
    assertz(city(Cidade,Latitude,Longitude)).

%  dist_cities(brussels,prague,D).
%  D = 716837.
dist_cities(C1,C2,Dist):-
    city(C1,Lat1,Lon1),
    city(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).

degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).

% distance(50.8462807,4.3547273,50.0878114,14.4204598,D).
% Online: http://www.movable-type.co.uk/scripts/latlong.html
%
outras_cidades_com_exclusoes(Cidade, CidadesExcluidas, Cidades):-
    findall(X, (city(X,_,_),X\==Cidade,\+member(X, CidadesExcluidas)), Cidades2),
    setof(Y, member(Y, Cidades2), Cidades).

num_cidades(N):-
    findall(X, city(X,_,_), C),
    length(C, N).

connect(X,Y):-
    dist_cities(X,Y,_);dist_cities(Y,X,_).

%DFS modificado. A única diferença é o "append" da Orig, cheeky
circuit(Orig,Dest,Cam):-
    dfs2(Orig,Dest,[Orig],Cam2),
    append(Cam2, [Orig], Cam).%,%Isto é mesmo cheeky, but I LOVE it
    %calc_distancia(Cam, Dist).
%condicao final: nodo actual = destino
dfs2(Dest,Dest,LA,Cam):-
    %caminho actual esta invertido
    %append(LA, Dest, LA2),
    reverse(LA,Cam).
dfs2(Act,Dest,LA,Cam):-
    connect(Act,X),
    \+ member(X,LA),
    dfs2(X,Dest,[X|LA],Cam).

% Determina TODOS os circuitos de Euler unicos possiveis desde uma
% determinada Origem e respetivas Distancias percorridas
tsp1_circuitos_unicos(Orig, CaminhosUnicos):-
    num_cidades(N),
    CompTotal is N+1,
    findall((Dist,Caminho),
            (   circuit(Orig,_,Caminho),
                length(Caminho,CompTotal),
                calc_distancia(Caminho, Dist)%SÓ calcula a distancia se o caminho for um Circuito
            ),
            Caminhos),
    setof(CaminhoUnico, member(CaminhoUnico, Caminhos), CaminhosUnicos).

%Calcula a Distancia percorrida num dado Caminho
calc_distancia(Last,0):- length(Last,1).
calc_distancia([First,Second|Caminho], Distancia):-
    calc_distancia([Second|Caminho], D1),
    dist_cities(First,Second,D),
    Distancia is D1+D.

tsp1(Orig, Distancia, Caminho):-
    tsp1_circuitos_unicos(Orig, Circuitos),
    nth0(0, Circuitos, (Distancia,Caminho)).


tsp2_vizinho_mais_proximo(Cidade, Cities, Vizinho, Dist):-
    %outras_cidades(Cidade, Cities),
    nth0(0, Cities, VizInit),
    dist_cities(Cidade, VizInit, DistInit),
    tsp2_vizinho_mais_proximo(Cidade, Cities, VizInit, DistInit, Vizinho, Dist),!.
%Cities contém os vizinhos de "Cidade" por explorar
% tsp2_vizinho_mais_proximo(Cidade, Cidades, Vizinho, DistAtual,
% DistMin)
tsp2_vizinho_mais_proximo(_, [], VizFin, DistFin, VizFin, DistFin).
% Caso o vizinho atual "H" esteja a uma distancia "Dist" menor que a
% "DistAtual" conhecida, copia no cabeçalho "Dist" para "DistMin"
tsp2_vizinho_mais_proximo(Cidade, [H|Cities], Vizinho, DistMin, V, D):-
    H \== Vizinho,
    dist_cities(Cidade, H, Dist),
    Dist < DistMin,
    tsp2_vizinho_mais_proximo(Cidade, Cities, H, Dist, V, D).
% Caso o anterior falhe, é para descartar o vizinho atual "_" e
% continuar com os dados existentes
tsp2_vizinho_mais_proximo(Cidade, [_|Cities], Vizinho, DistMin, V, D):-
    tsp2_vizinho_mais_proximo(Cidade, Cities, Vizinho, DistMin, V, D).

list_empty([], true).
list_empty([_|_], false).

tsp2(Orig, Circuito, DistTotal):-
    %Determina um Caminho, começando em "[Orig]"
    tsp2_priv(Orig, [Orig], 0, Caminho, DistTotal1),
    last(Caminho, Penultimo),
    outras_cidades_com_exclusoes(Penultimo, Caminho, Vizinhos),
    tsp2_vizinho_mais_proximo(Penultimo, Vizinhos, Ultimo, DistPen),
    append(Caminho, [Ultimo], Caminho1),
    dist_cities(Ultimo, Orig, DistUlt),
    append(Caminho1, [Orig], Circuito),
    DistTotal is DistTotal1 + DistPen + DistUlt.
tsp2_priv(_, CidadesVisitadas, DistTotal, CidadesVisitadas, DistTotal):-
    list_empty(CidadesVisitadas, false),
    num_cidades(N),
    N1 is N-1,
    length(CidadesVisitadas, N1).
tsp2_priv(CidadeAtual, CidadesVisitadas, DistTotal, C, D):-
    outras_cidades_com_exclusoes(CidadeAtual, CidadesVisitadas, Vizinhos),
    tsp2_vizinho_mais_proximo(CidadeAtual, Vizinhos, Vizinho, Dist),
    DistTotal1 is DistTotal + Dist,
    append(CidadesVisitadas, [Vizinho], CidadesVisitadas1),
    tsp2_priv(Vizinho, CidadesVisitadas1, DistTotal1, C, D).



%Codigo do professor Antonio Silva, disponivel na página DEI dele
% Given three colinear points p, q, r, checks if point q lies on line segment 'pr'
% onSegment(P, Q, R)

onSegment((PX,PY), (QX,QY), (RX,RY)):-
    QX =< max(PX,RX),
    QX >= min(PX,RX),
    QY =< max(PY,RY),
    QY >= min(PY,RY).


% To find orientation of ordered triplet (p, q, r).
% It returns following values:
% 0 --> p, q and r are colinear
% 1 --> Clockwise
% 2 --> Counterclockwise

orientation((PX,PY), (QX,QY), (RX,RY), Orientation):-
	Val is (QY - PY) * (RX - QX) - (QX - PX) * (RY - QY),
	(
		Val == 0, !, Orientation is 0;
		Val > 0, !, Orientation is 1;
		Orientation is 2
	).

orientation4cases(P1,Q1,P2,Q2,Or1,Or2,Or3,Or4):-
    orientation(P1, Q1, P2,Or1),
    orientation(P1, Q1, Q2,Or2),
    orientation(P2, Q2, P1,Or3),
    orientation(P2, Q2, Q1,Or4).


% Verifies if line segment 'p1q1' and 'p2q2' intersect.

doIntersect(P1,Q1,P2,Q2):-
    % Find the four orientations needed for general and special cases
	orientation4cases(P1,Q1,P2,Q2,Or1,Or2,Or3,Or4),
	(
    % General case
    Or1 \== Or2 , Or3 \== Or4,!;

    % Special Cases
    % p1, q1 and p2 are colinear and p2 lies on segment p1q1
    Or1 == 0, onSegment(P1, P2, Q1),!;

    % p1, q1 and p2 are colinear and q2 lies on segment p1q1
    Or2 == 0, onSegment(P1, Q2, Q1),!;

    % p2, q2 and p1 are colinear and p1 lies on segment p2q2
    Or3 == 0, onSegment(P2, P1, Q2),!;

     % p2, q2 and q1 are colinear and q1 lies on segment p2q2
    Or4 == 0, onSegment(P2, Q1, Q2),!
    ).
%FIM do codigo do professor

% Verifica se o par de cidades "(Cidade1,Cidade2)" intersecta com o par
% "(Cidade3,Cidade4)"
doIntersect([Cidade1,Cidade2], [Cidade3,Cidade4]):-
    %Os nós de ligação no caminho têm que ser diferentes
    dif(Cidade1, Cidade2),
    dif(Cidade1, Cidade3),
    dif(Cidade1, Cidade4),
    dif(Cidade2, Cidade3),
    dif(Cidade2, Cidade4),
    dif(Cidade3, Cidade4),
    %Descobrir coordenadas das 4 Cidades
    city(Cidade1, C1_lat, C1_lon),
    city(Cidade2, C2_lat, C2_lon),
    city(Cidade3, C3_lat, C3_lon),
    city(Cidade4, C4_lat, C4_lon),

    doIntersect((C1_lat,C1_lon), (C2_lat,C2_lon), (C3_lat,C3_lon), (C4_lat,C4_lon)).

%----------------------------------------------------------------------------------------------------
% rGraph(Origin,UnorderedListOfEdges,OrderedListOfEdges)
%
% Examples:
% ---------
% ?- rGraph(a,[[a,b],[b,c],[c,d],[e,f],[d,f],[e,a]],R).
%
% ?- rGraph(brussels,[[vienna, sarajevo], [sarajevo, tirana],[tirana,sofia], [sofia, minsk], [andorra,brussels],[brussels,minsk],[vienna,andorra]],R).
%
%
rGraph(Orig,[[Orig,Z]|R],R2):-!,
	reorderGraph([[Orig,Z]|R],R2).
rGraph(Orig,R,R3):-
	member([Orig,X],R),!,
	delete(R,[Orig,X],R2),
	reorderGraph([[Orig,X]|R2],R3).
rGraph(Orig,R,R3):-
	member([X,Orig],R),
	delete(R,[X,Orig],R2),
	reorderGraph([[Orig,X]|R2],R3).


reorderGraph([],[]).

reorderGraph([[X,Y],[Y,Z]|R],[[X,Y]|R1]):-
	reorderGraph([[Y,Z]|R],R1).

reorderGraph([[X,Y],[Z,W]|R],[[X,Y]|R2]):-
	Y\=Z,
	reorderGraph2(Y,[[Z,W]|R],R2).

reorderGraph2(_,[],[]).
reorderGraph2(Y,R1,[[Y,Z]|R2]):-
	member([Y,Z],R1),!,
	delete(R1,[Y,Z],R11),
	reorderGraph2(Z,R11,R2).
reorderGraph2(Y,R1,[[Y,Z]|R2]):-
	member([Z,Y],R1),
	delete(R1,[Z,Y],R11),
	reorderGraph2(Z,R11,R2).

% Converte um "Circuito" na forma [Cidade1, Cidade2, Cidade3, ...
% CidadeN, Cidade1] numa lista de edges "EdgeList" na forma
% [(Cidade1,Cidade2), (Cidade2,Cidade3), ... , (CidadeN, Cidade1)]
convert_circuito_to_edges(Circuito, EdgeList):-
    convert_circuito_to_edges(Circuito, [], EdgeList).
convert_circuito_to_edges(Circuito, EdgeList, EdgeList):-
    length(Circuito, 1).
convert_circuito_to_edges([First,Second|Circuito], EdgeList, EL):-
    append(EdgeList, [[First, Second]], NewEdgeList),
    convert_circuito_to_edges([Second|Circuito], NewEdgeList, EL).

/*convert_edges_to_circuito(EdgeList, Circuito):-
    convert_edges_to_circuito(EdgeList, [], Circuito).
convert_edges_to_circuito([[_|FinalCidade]|EdgeList], Circuito, FinalCircuito):-
    length(EdgeList, 0),
    append(Circuito, FinalCidade, FinalCircuito).
convert_edges_to_circuito([[Cidade|_]|EdgeList], Circuito, C):-
    append(Circuito, Cidade, NewCircuito),
    convert_circuito_to_edges(EdgeList, NewCircuito, C).*/
convert_edges_to_circuito([[P1,P2]|[]], [P1|[P2]]) :- !.
convert_edges_to_circuito([[P1,_]|T1], [P1|T2]) :- convert_edges_to_circuito(T1, T2).

% Encontra a 1ª edge "Intersect" em "EdgeList" que intersecta com a
% "Edge" dada
% find_first_intersect(Edge, EdgeList, Intersect).
find_first_intersect(_, [], _):- false.
find_first_intersect(Edge, [NextEdge|_], NextEdge):-
    doIntersect(Edge, NextEdge).
    %(doIntersect(Edge, H) -> (Intersect = H),true);
    %find_first_intersect(Edge, EdgeList, Intersect).
find_first_intersect(Edge, [_|EdgeList], Intersect):-
    find_first_intersect(Edge, EdgeList, Intersect).

% Percorre toda a "AuxEdgeList" à procura de uma edge "H" que intersecte
% com aquelas pertencentes à "EdgeMasterList".
tsp3_find_first_intersect([], _, _):- false.
tsp3_find_first_intersect([H|_], EdgeMasterList, (H,[Node3,Node4])):-
    find_first_intersect(H, EdgeMasterList, [Node3,Node4]).
    %->(/*(Node3,Node4) = IntersectingEdge, */Result = (H,(Node3,Node4)),true);

    %tsp3_find_first_intersect(AuxEdgeList, EdgeMasterList, Result).
tsp3_find_first_intersect([_|AuxEdgeList], EdgeMasterList, Result):-
    tsp3_find_first_intersect(AuxEdgeList, EdgeMasterList, Result).

%Replace 1st occurrence of element "H" with "X"
/*replace(_, _, [], []).
replace(O, R, [O|T], [R|T]). %:- replaceP(O, R, T, T2).
replace(O, R, [H|T], [H|T2]) :- dif(H,O), replace(O, R, T, T2).*/

tsp3_get_origin([[Origin|_]|_], Origin).

tsp3_remove_intersection(Intersection, EdgeList, NewEdgeList):-
    ([Cidade1,Cidade2],[Cidade3,Cidade4]) = Intersection,

    %Remover as Edges que formam a intersecção da lista de Edges
    OldEdge1 = [Cidade1,Cidade2], OldEdge2 = [Cidade3, Cidade4],
    delete(EdgeList, OldEdge1, AuxList1),
    delete(AuxList1, OldEdge2, AuxList2),

    %Trocar os nós interiores (Cidade2 e Cidade3) para formar o melhor par de novas Edges
    NewEdge1 = [Cidade1,Cidade3], NewEdge2 = [Cidade2, Cidade4],
    append(AuxList2, [NewEdge1], AuxList3),
    append(AuxList3, [NewEdge2], AuxList4),
    tsp3_get_origin(EdgeList, Origin),
    rGraph(Origin, AuxList4, NewEdgeList).

% Condicao paragem: não existem mais intersecções, logo copia "EdgeList"
% para o resultado
tsp3_any_intersections(EdgeList, EdgeList):-
    %convert_circuito_to_edges(Circuito, EdgeList),
    \+tsp3_find_first_intersect(EdgeList, EdgeList, _).
% Enquanto existirem "Edges" que se intersectam, trocar as posições dos
% nós interiores dessas "Edges" no "Circuito", formando um
% "NovoCircuito" sem essa intersecção
tsp3_any_intersections(EdgeList, Result):-
    %convert_circuito_to_edges(Circuito, EdgeList),
    tsp3_find_first_intersect(EdgeList, EdgeList, Intersection),

    %IMPORTANTE: começar por substituir "NoInterior2" por "NoInterior1". O replace começa da esq para a direita, por isso é preciso substituir 1º o que está mais à direita.
    %replace(NoInterior2, NoInterior1, Circuito, Aux),
    %replace(NoInterior1, NoInterior2, Aux, NovoCircuito),
    tsp3_remove_intersection(Intersection, EdgeList, NewEdgeList),
    tsp3_any_intersections(NewEdgeList, Result).

tsp3(Orig, Circuito, DistTotal):-
    tsp2(Orig, CircuitoTsp2, _),
    convert_circuito_to_edges(CircuitoTsp2, EdgeList),
    tsp3_any_intersections(EdgeList, CleanEdgeList),
    convert_edges_to_circuito(CleanEdgeList, Circuito),
    calc_distancia(Circuito, DistTotal).









