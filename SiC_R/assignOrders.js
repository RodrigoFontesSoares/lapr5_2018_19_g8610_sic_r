const swipl = require('swipl-stdio');
const { list, compound, variable, dict, serialize } = swipl.term;

// Engine represents on SWI-Prolog process.
const engine = new swipl.Engine();

//----------------------String processing
/**
 * WARNING: do NOT mess around with this function, you stupid
 * son of a bitch. This piece of shit makes the WHOLE DAMN thing work.
 * 
 * This function exists because motherfucking "swipl-stdio"
 * adds a bunch of "\" and " ' " when compounding predicates.
 * This removes those useless chars
 * @param {*} assertCityPredicate 
 */
function assertCleanup(assertPredicate) {
    assertPredicate = assertPredicate.replace(/\\/g, "");
    assertPredicate = assertPredicate.replace(/\'/g, "");
    return assertPredicate;
}

function OutputCleanup(circuitOutput){
    circuitOutput = circuitOutput.replace(/"head":/g, "");
    circuitOutput = circuitOutput.replace(/"tail":{/g, "");
    circuitOutput = circuitOutput.replace(/,\"tail\":"\[\]\"/g, '}');

    //Remove all right-facing curly braces and replace them with a single one afterwards to close the JSON object
    circuitOutput = circuitOutput.replace(/}+/g, "}");
    circuitOutput = circuitOutput.replace(/,{/g, ",{\"Order\":");
    circuitOutput = circuitOutput.replace(/{{/g, "{{\"Order\":");
    circuitOutput = circuitOutput.replace(/,\"/g, ",\"Factory\":\"");
    circuitOutput = circuitOutput.replace(/{{/g, "{\"Assignments\":[{");
    circuitOutput = circuitOutput.concat(']}');
    return circuitOutput;
}

//Call para definir o working directory
async function ensureWorkingDirectory(engine) {
    const dirQuery = await engine.call('working_directory(_, .)');
    if (dirQuery) {
        console.log(`dirQuery sucess`);
    } else {
        console.log('dirQuery Call failed.');
    }
}

//Consultar o ficheiro PROLOG (assignOrders.pl)
async function consultFile(engine) {
    const consultQuery = await engine.call('consult("assignOrders")');
    if (consultQuery) {
        console.log(`consultQuery success:`);
    } else {
        console.log('consultQuery Call failed.');
    }
}

async function clearOrders(engine){
    clearQuery = await engine.call('retractall(order(_, _, _))');
    if (clearQuery) {
        console.log(`clearQuery success:`);
    } else {
        console.log('clearQuery Call failed.');
    }
}

async function clearFactories(engine){
    clearQuery = await engine.call('retractall(factory(_, _, _))');
    if (clearQuery) {
        console.log(`clearQuery success:`);
    } else {
        console.log('clearQuery Call failed.');
    }
}

/**
 * Asserts the existing City (contained in "orders")
 * in the Knowledgebase of the given PROLOG "engine"
 * @param {The working PROLOG engine} engine 
 * @param {A JSON object representing all Orders} orders 
 */
async function assertOrders(engine, orders){
    for(var i = 0; i < orders.length; i++){
        var order = orders[i];
        var id = order._id;
        var latitude = parseFloat(order.deliveryAddress.city.location.latitude);
        var longitude = parseFloat(order.deliveryAddress.city.location.longitude);

        await assertOrder(engine, id, latitude, longitude);

    }
}

/**
 * Asserts a City with a given name, latitude and longitude
 * in a given engine
 * @param {The working PROLOG engine} engine 
 * @param {*} name 
 * @param {*} latitude 
 * @param {*} longitude 
 */
async function assertOrder(engine, name, latitude, longitude){
    var cityPredicate = serialize(
        compound('assertOrder', [
            name,
            latitude,
            longitude]
        )
    );
    console.log(`cityPredicate ${cityPredicate}`);

    const cityQuery = await engine.call(cityPredicate);
    if (cityQuery) {
        console.log(`cityQuery ${name} success`);
    } else {
        console.log(`cityQuery ${name} Call failed.`);
    }
}

/**
 * Asserts the existing Factory (contained in "orders")
 * in the Knowledgebase of the given PROLOG "engine"
 * @param {The working PROLOG engine} engine 
 * @param {A JSON object representing all Factory} factories 
 */
async function assertFactories(engine, factories){
    for(var i = 0; i < factories.length; i++){
        var factory = factories[i];
        var name = factory.name;
        var latitude = parseFloat(factory.location.latitude);
        var longitude = parseFloat(factory.location.longitude);

        await assertFactory(engine, name, latitude, longitude);

    }
}

/**
 * Asserts a Factory with a given name, latitude and longitude
 * in a given engine
 * @param {The working PROLOG engine} engine 
 * @param {*} name 
 * @param {*} latitude 
 * @param {*} longitude 
 */
async function assertFactory(engine, name, latitude, longitude){
    var factoryPredicate = serialize(
        compound('assertFactory', [
            name,
            latitude,
            longitude]
        )
    );
    console.log(`factoryPredicate ${factoryPredicate}`);

    const cityQuery = await engine.call(factoryPredicate);
    if (cityQuery) {
        console.log(`cityQuery ${name} success`);
    } else {
        console.log(`cityQuery ${name} Call failed.`);
    }
}

async function callAssignOrders(engine, res) {
    var queryPredicate = serialize(
        compound('assignOrders', [
            variable('Assignments')]
        )
    );
    console.log(`queryPredicate ${queryPredicate}`);

    var response=[];
    const tspQuery = await engine.call(queryPredicate);
    if (tspQuery) {
        //console.log(`Assignments in raw form: ${tspQuery.Assignments}`);
        response = OutputCleanup(JSON.stringify(tspQuery.Assignments));
       console.log(`Assignments in JSON: ${response}`);

    } else {
        console.log('AssignOrders Call failed.');
        response =  'AssignOrders Call failed.';
    }
    res.send(response);
}

var assign = {
    assignOrder: function (orders, factories, res) {
        const engine = new swipl.Engine();

        (async () => {
            await ensureWorkingDirectory(engine);
            await consultFile(engine);
            await clearOrders(engine);
            await clearFactories(engine);
            await assertOrders(engine, orders);
            await assertFactories(engine, factories);

            await callAssignOrders(engine, res);

            engine.close();
        })().catch((err) => console.log(err));
    }
}

module.exports = assign;