:-include('assignOrders.pl').

run_tests(Res):-    testAssertFactory(a,10,10),
                    testAssertOrder(b,20,20),
                    testSame_Order_Factory_Distance(),
                    testDifferent_Order_Factory_Distance(),
                    testListAllOrders(),
                    testAssignOrdersLength(),
                    testAssignOrders(),
                    testDist_OrderFactory(R1),
                    R1 = 'true',
                    Res = 'true',!.

run_tests('false').

testAssertFactory(Factory,Latitude,Longitude):-
    % After asserting a Factory with given Latitude and Longitude,
    % the corresponding "factory/3" predicate must be true.
    assertFactory(Factory,Latitude,Longitude),
    factory(Factory,Latitude,Longitude).

testAssertOrder(Order,Latitude,Longitude):-
    % After asserting a Order with given Latitude and Longitude,
    % the corresponding "order/3" predicate must be true.
    assertOrder(Order,Latitude,Longitude),
    order(Order,Latitude,Longitude).

%Expected false
%A encomenda e fabrica estão atribuitas com a mesma latitude e longitude logo mesma cidade MAS
% Falso é esperado devido a ordem de parametro estarem trocadas 'dist_OrderFactory(Order,Factory,Dist)';
% 'retractall' retira qualquer fabrica e encomenda que possa ter na BC para desta forma ser possivel
% correr sempre o mesmo teste em qualquer altura do projecto e esperar sempre o mesmo resultado
testDist_OrderFactory(R):-  retractall(factory(_,_,_)),
                            retractall(order(_,_,_)),
                            assertFactory(y,10.11,10.11),
                            assertOrder(x,10.11,10.11),
                            dist_OrderFactory(y,x,_),
                            R == 'false'.

testDist_OrderFactory('true').                           

%Expected true
testSame_Order_Factory_Distance():-  retractall(order(_,_,_)), 
                                            retractall(factory(_,_,_)),
                                            assertOrder(a,10,20),
                                            assertFactory(b,10,20),
                                            dist_OrderFactory(a,b,Dist), Dist == 0. 

%Expected true
testDifferent_Order_Factory_Distance():-   retractall(order(_,_,_)),
                                            retractall(factory(_,_,_)), 
                                            assertOrder(a,10,20),
                                            assertFactory(b,30,40),
                                            dist_OrderFactory(a,b,Dist), Dist \= 0.                                        

%Expected true
testListAllOrders():-   retractall(order(_,_,_)), 
                        assertOrder(a,10,20),
                        assertOrder(b,30,40),
                        assertOrder(c,50,60),
                        findall(O, order(O,_,_), Orders), length(Orders,N_Orders), 
                        N_Orders == 3.

%Expected true
testAssignOrdersLength():-              retractall(order(_,_,_)),
                                        retractall(factory(_,_,_)), 
                                        assertOrder(a,10,20),
                                        assertOrder(b,30,40),
                                        assertOrder(c,50,60),
                                        assertFactory(b,30,40),
                                        assignOrders(Orders_Factory),
                                        list_length(Orders_Factory,N), 
                                        N == 3.        

%Predicado criado para a verificação do teste em cima apresentado 
list_length([] , 0 ).
list_length([_|Xs] , L ) :- list_length(Xs,N) , L is N+1 .         

%Expected true
%Verifica se todos os elementos da lista estão atribuidos como [[encomenda,fabrica]|T],
% verificando tb a sua associação como encomenda e fabrica na propria BC.
testAssignOrders():-                    retractall(order(_,_,_)),
                                        retractall(factory(_,_,_)), 
                                        assertOrder(a,10,20),
                                        assertOrder(b,30,40),
                                        assertOrder(c,50,60),
                                        assertFactory(b,30,40),
                                        assignOrders(Orders_Factory),
                                        verify(Orders_Factory).

verify([]).
verify([[O,F]|T]):-  order(O,_,_),
                    factory(F,_,_),
                    verify(T).                                   