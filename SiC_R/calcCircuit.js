const swipl = require('swipl-stdio');
const { list, compound, variable, dict, serialize } = swipl.term;

// Engine represents on SWI-Prolog process.
const engine = new swipl.Engine();
//----------------------String processing
/**
 * WARNING: do NOT mess around with this function, you stupid
 * son of a bitch. This piece of shit makes the WHOLE DAMN thing work.
 * 
 * This function exists because motherfucking "swipl-stdio"
 * adds a bunch of "\" and " ' " when compounding predicates.
 * This removes those useless chars
 * @param {*} assertCityPredicate 
 */
function assertCleanup(assertPredicate){
    assertPredicate = assertPredicate.replace(/\\/g, "");
    assertPredicate = assertPredicate.replace(/\'/g, "");
    return assertPredicate;
}

function circuitOutputCleanup(circuitOutput){
    circuitOutput = circuitOutput.replace(/"head":/g, "");
    circuitOutput = circuitOutput.replace(/"tail":{/g, "");
    circuitOutput = circuitOutput.replace(/,\"tail\":"\[\]\"/g, "");

    //Remove all right-facing curly braces and replace them with a single one afterwards to close the JSON object
    circuitOutput = circuitOutput.replace(/}/g, "");
    circuitOutput = circuitOutput.concat('}');
    return circuitOutput;
}

async function listCityPredicates(engine){
    const listQuery = await engine.call('listing(city)');
    if (listQuery) {
        console.log(`listQuery sucess`);
    } else {
        console.log('listQuery Call failed.');
    }
}

async function ensureWorkingDirectory(engine){
    const dirQuery = await engine.call('working_directory(_, .)');
    if (dirQuery) {
        console.log(`dirQuery sucess`);
    } else {
        console.log('dirQuery Call failed.');
    }
}

async function consultFile(engine){
    const consultQuery = await engine.call('consult("TSP")');
    if (consultQuery) {
        console.log(`consultQuery success:`);
    } else {
        console.log('consultQuery Call failed.');
    }
}

/**
 * Asserts the existing Locations (contained in "orders")
 * in the Knowledgebase of the given PROLOG "engine"
 * @param {The working PROLOG engine} engine 
 * @param {A JSON object representing all Orders} orders 
 */
async function assertLocations(engine, orders){
    for(var i = 0; i < orders.length; i++){
        var order = orders[i];
        var city = order.deliveryAddress.city.name;
        var latitude = parseFloat(order.deliveryAddress.city.location.latitude);
        var longitude =  parseFloat(order.deliveryAddress.city.location.longitude);

        await assertLocation(engine, city, latitude, longitude);

        var factory = order.factory.name;
        latitude = parseFloat(order.factory.location.latitude);
        longitude =  parseFloat(order.factory.location.longitude);

        await assertLocation(engine, factory, latitude, longitude);
    }
    await listCityPredicates(engine);
}

/**
 * Asserts a Location with a given name, latitude and longitude
 * in a given engine
 * @param {The working PROLOG engine} engine 
 * @param {*} name 
 * @param {*} latitude 
 * @param {*} longitude 
 */
async function assertLocation(engine, name, latitude, longitude){
    var cityPredicate = serialize(
        compound('assertCity', [
            name,
            latitude,
            longitude]
        )
    );
    console.log(`cityPredicate ${cityPredicate}`);

    /*var assertCityPredicate = serialize(
        compound('assertCity', [
            cityPredicate]
        )
    );*/
    /*console.log(`assertCityPredicate ${JSON.stringify(assertCityPredicate)}`);
    console.log(`assertCityPredicate ${assertCityPredicate}`);
    assertCityPredicate = assertCleanup(assertCityPredicate);
    console.log(`assertCityPredicate ${JSON.stringify(assertCityPredicate)}`);
    console.log(`assertCityPredicate ${assertCityPredicate}`);*/
    
    const cityQuery = await engine.call(cityPredicate);
    if (cityQuery) {
        console.log(`cityQuery ${name} success`);
        await listCityPredicates(engine);
    } else {
        console.log(`cityQuery ${name} Call failed.`);
    }
}

async function clearLocations(engine){
    await listCityPredicates(engine);
    /*var retractAllCitiesPredicate = serialize(
        compound('retractall', [
            'city(_, _, _)']
        )
    );
    console.log(`retractAllCitiesPredicate ${retractAllCitiesPredicate}`);*/
    
    clearQuery = await engine.call('retractall(city(_, _, _))');
    if (clearQuery) {
        console.log(`clearQuery success:`);
    } else {
        console.log('clearQuery Call failed.');
    }
    await listCityPredicates(engine);
}

async function callTSP(engine, origin, res){
    var queryPredicate = serialize(
        compound('tsp3', [
            origin,
            variable('Circ'),
            variable('Dist')]
        )
    );
    console.log(`tsp3queryPredicate ${queryPredicate}`);

    var response=[];
    const tspQuery = await engine.call(queryPredicate);
    if (tspQuery) {
        console.log(`Distance is: ${tspQuery.Dist}`);
        console.log(`Circuit is: ${JSON.stringify(tspQuery.Circ)}`);
        var circuit = circuitOutputCleanup(JSON.stringify(tspQuery.Circ));

        response = {
            distance : tspQuery.Dist,
            circuit : circuit
        }
    } else {
        console.log('TSP Call failed.');
        response =  'TSP Call failed.';
    }
    console.log(response);
    res.send(response);
}

var calcCircuit = {
    calcCircuitTSP : function(orders, res){
        const engine = new swipl.Engine();

        (async () => {
            await ensureWorkingDirectory(engine);
            await consultFile(engine);
            await clearLocations(engine);
            await assertLocations(engine, orders);

            var origin = orders[0].factory.name;
            await callTSP(engine, origin, res);

            engine.close();
        })().catch((err) => console.log(err));
    }
}

module.exports = calcCircuit;