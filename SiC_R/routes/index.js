var express = require('express');
var router = express.Router();

const calcCircuit = require('../calcCircuit');
const assignOrders = require('../assignOrders');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/calcCircuit', function (req, res) {
  var orders = req.body.orders;
  
  //This function takes care of sending the response
  calcCircuit.calcCircuitTSP(orders, res);
});

router.post('/assignOrders', function (req, res) {
    //all orders in the json body
    var orders = req.body.orders;
    //all factories in the json body
    var factories = req.body.factories;
    
    //This function takes care of sending the response
    assignOrders.assignOrder(orders, factories, res);
});

module.exports = router;
