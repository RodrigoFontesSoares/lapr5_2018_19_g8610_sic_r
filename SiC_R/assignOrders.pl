:-dynamic dist/3.
:-dynamic city/3.

%order(id,lat,long)
:-dynamic factory/3.
%order(id,lat,long)
:-dynamic order/3.

:-dynamic(assignedOrders/2).

/*
%BC
%Test PROLOG same in the JSON 
%city(name,latitude,longitude)
order(brussels,50.8462807,4.3547273). 
order(porto,41.1621376,-8.6569732). 
order(tirana,41.33165,19.8318). 
order(london,51.5001524,-0.1262362).
order(paris,48.8566667,2.3509871).
order(tbilisi,41.709981,44.792998).
order(athens,37.97918,23.716647).
order(budapest,47.4984056,19.0407578).
order(dublin,53.344104,-6.2674937).
order(rome,41.8954656,12.4823243).
order(riga,56.9465346,24.1048525).
order(vaduz,47.1410409,9.5214458).
order(luxembourg,49.815273,6.129583).
order(skopje,42.003812,21.452246).
order(monaco,43.750298,7.412841).
order(warsaw,52.2296756,21.0122287).
order(moscow,55.755786,37.617633).
order(edinburgh,55.9501755,-3.1875359).
order(belgrade,44.802416,20.465601).
order(ljubljana,46.0514263,14.5059655).
order(stockholm,59.3327881,18.0644881).
order(bern,46.9479986,7.4481481).
order(cardiff,51.4813069,-3.1804979).

factory(athens,37.97918,23.716647).
factory(moscow,55.755786,37.617633).
factory(brussels,50.8462807,4.3547273).
factory(porto,41.1621376,-8.6569732).
factory(sarajevo,43.85643,18.41342).
*/

%Cria uma lista com todas as fabricas
listFactory(ListFactories):- findall(F, factory(F,_,_), ListFactories).

%Cria uma lista com todas as cidades que estão com as encomendas validadas
%NOTA: Neste momento vai listar todas já que ainda não relacionamos com as validaçoes das encomendas
listCity(ValidatedOrders):- findall(C, city(C,_,_), ValidatedOrders).

listAllOrders(Orders):- findall(O, order(O,_,_), Orders).
 
assignOrders(Orders_Factory):-      listAllOrders(Orders),
                                    nearestFactory(Orders, [], Orders_Factory).

nearestFactory([], Result, Result).
nearestFactory([Order|T], OrderFactoryList, Result):-
                                findall( (Dist,Factory), (factory(Factory,_,_),dist_OrderFactory(Order,Factory,Dist)) ,ListAux),
                                sort(ListAux,ListSort), nth0(0,ListSort,(_,FactoryOrder)),
                                append(OrderFactoryList, [[Order,FactoryOrder]], NewOrderFactoryList),
                                nearestFactory(T, NewOrderFactoryList, Result).


%Prevents duplicate insertions of Factory. Use THIS instead of "assert"
assertFactory(Factory,Latitude,Longitude):-
    \+factory(Factory,_,_),
    assertz(factory(Factory,Latitude,Longitude)).

%Prevents duplicate insertions of Order. Use THIS instead of "assert"
assertOrder(Order,Latitude,Longitude):-
    \+order(Order,_,_),
    assertz(order(Order,Latitude,Longitude)).

%  O de uma encomenda para um F (fabrica)
dist_OrderFactory(O,F,Dist):-
    order(O,Lat1,Lon1),
    factory(F,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).

degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).