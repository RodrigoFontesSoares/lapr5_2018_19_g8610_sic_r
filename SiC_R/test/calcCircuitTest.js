const assert = require('assert');
const swipl = require('swipl-stdio');
const engine = new swipl.Engine();
const consultQuery = engine.call('consult("testCalcCircuitOrders.pl")');

describe('TSP tests', function () {

    /*it('should run all tests', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('run_tests(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });*/

    it('Test outras_cidades_com_exclusoes', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_outras_cidades_com_exclusoes(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test tsp2_vizinho_mais_proximo', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_tsp2_vizinho_mais_proximo(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test tsp2', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_tsp2(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test convert_circuito_to_edges', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_convert_circuito_to_edges(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test tsp3_find_first_intersect', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_tsp3_find_first_intersect(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test tsp3_remove_intersection', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_tsp3_remove_intersection(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test convert_edges_to_circuito', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_convert_edges_to_circuito(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test calc_distancia', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_calc_distancia(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });

    it('Test tsp3', async () => {
        await engine.call('working_directory(_, .)');
        
        console.log(consultQuery);

        const query = await engine.createQuery('test_tsp3(Res)');
        try {
            const result = await query.next();
            assert.equal(result.Res, true);
        } finally {
            await query.close();
        }
    });
});