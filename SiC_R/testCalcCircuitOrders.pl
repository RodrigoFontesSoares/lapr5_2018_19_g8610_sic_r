:-include('TSP.pl').
	
test_outras_cidades_com_exclusoes(Cidade,CidadesExcluidas,ExpectedCidades):-
	outras_cidades_com_exclusoes(Cidade,CidadesExcluidas,Cidades),
	length(Cidades,ExpectedCidades).
	
test_outras_cidades_com_exclusoes(Res):-
	write('Lista de outras cidades vai conter 48 cidades.'), nl,
	test_outras_cidades_com_exclusoes(brussels,[tirana,skopje],48),
	write('Lista de outras cidades vai conter 50 cidades.'), nl,
	test_outras_cidades_com_exclusoes(podgorica,[],50),
	findall(C,city(C,_,_),L),
	write('Esperado o valor falso [not(false) = true] pois a lista sera vazia.'), nl, nl,
	not(test_outras_cidades_com_exclusoes(cardiff,L,_)),
	write('Test outras_cidades_com_exclusoes passed!'), nl, nl,
	Res = true, !.

test_outras_cidades_com_exclusoes(false).
	
test_tsp2_vizinho_mais_proximo(Cidade,Cities,ExpectedVizinho,ExpectedDist):-
	tsp2_vizinho_mais_proximo(Cidade,Cities,ExpectedVizinho,ExpectedDist).
	
test_tsp2_vizinho_mais_proximo(Res):-
	write('Vizinho mais proximo de brussels entre athens e -> london, distancia 320686.'), nl,
	test_tsp2_vizinho_mais_proximo(brussels,[athens,london],london,320686),
	findall(C,(city(C,_,_), C \== brussels),L),
	write('Vizinho mais proximo de brussels e luxembourg, distancia 170328.'), nl,
	test_tsp2_vizinho_mais_proximo(brussels,L,luxembourg,170328),
	write('Esperado o valor falso [not(false) = true] pois nao tem vizinhos.'), nl, nl,
	not(test_tsp2_vizinho_mais_proximo(vienna,[],_,_)),
	write('Test tsp2_vizinho_mais_proximo passed!'), nl, nl,
	Res = true, !.

test_tsp2_vizinho_mais_proximo(false).
	
test_tsp2(Orig,ExpectedCircuito,ExpectedDistTotal):-
	tsp2(Orig,Circuito,ExpectedDistTotal),
	length(Circuito,ExpectedCircuito).
	
test_tsp2(Res):-
	write('Circuito a partir de pristina percorrendo todas as cidades tem 52 elementos e distancia 29737521.'), nl,
	test_tsp2(pristina,52,29737521),
	write('kinshasa nao existe na lista de cidades.'), nl,
	not(test_tsp2(kinshasa,_,_)),
	write('Impossivel calcular o circuito sem fornecer uma cidade.'), nl, nl,
	not(test_tsp2(_,_,_)),
	write('Test tsp2 passed!'), nl, nl,
	Res = true, !.

test_tsp2(false).
	
test_convert_circuito_to_edges(Circuito,ExpectedEdgeList):-
	convert_circuito_to_edges(Circuito,ExpectedEdgeList).
	
test_convert_circuito_to_edges(Res):-
	write('Circuito [tirana,podgorica,tirana], output [[tirana,podgorica],[podgorica,tirana]]'), nl,
	test_convert_circuito_to_edges([tirana,podgorica,tirana],[[tirana,podgorica],[podgorica,tirana]]),
	write('Circuito [madrid,madrid], output [[madrid,madrid]]'), nl,
	test_convert_circuito_to_edges([madrid,madrid],[[madrid,madrid]]),
	write('Esperado o valor falso [not(false) = true] pois nao pode converter lista vazia.'), nl, nl,
	not(test_convert_circuito_to_edges([],_)),
	write('Test convert_circuito_to_edges passed!'), nl, nl,
	Res = true, !.

test_convert_circuito_to_edges(false).

test_tsp3_find_first_intersect(AuxEdgeList,EdgeMasterList,ExpectedResult):-
	tsp3_find_first_intersect(AuxEdgeList,EdgeMasterList,ExpectedResult).
	
test_tsp3_find_first_intersect(Res):-
	write('Primeira intersecao de [paris,berlin] com segmentos [madrid,lisbon],->[rome,london],[reykjavik,vienna]'), nl,
	test_tsp3_find_first_intersect([[paris,berlin]],[[madrid,lisbon],[rome,london],[reykjavik,vienna]],([paris,berlin],[rome,london])),
	write('[cardiff,amsterdam] nao interseta com segmentos [madrid,lisbon],[skopje,moscow]'), nl,
	not(test_tsp3_find_first_intersect([[cardiff,amsterdam]],[[madrid,lisbon],[skopje,moscow]],_)),
	write('Esperado o valor falso [not(false) = true] pois lista de segmentos nao contem nenhum elemento.'), nl, nl,
	not(test_tsp3_find_first_intersect([],[],_)),
	write('Test tsp3_find_first_intersect passed!'), nl, nl,
	Res = true, !.

test_tsp3_find_first_intersect(false).

test_tsp3_remove_intersection(Intersect,EdgeList,ExpectedNewEdgeList):-
	tsp3_remove_intersection(Intersect,EdgeList,ExpectedNewEdgeList).
	
test_tsp3_remove_intersection(Res):-
	write('Resultado esperado: [[paris, reykjavik], [reykjavik, berlin], [berlin, vienna], [vienna, paris]].'), nl,
	test_tsp3_remove_intersection(([paris,berlin],[reykjavik,vienna]),[[paris,berlin],[berlin,reykjavik],[reykjavik,vienna],[vienna,paris]],[[paris, reykjavik], [reykjavik, berlin], [berlin, vienna], [vienna, paris]]),
	write('Segmentos da intersecao([cardiff,amsterdam],[talinn,helsinki]) nao estao contidos na lista de segmentos.'), nl,
	not(test_tsp3_remove_intersection(([cardiff,amsterdam],[talinn,helsinki]),[[madrid,lisbon],[skopje,moscow]],_)),
	write('Esperado o valor falso [not(false) = true] pois lista de segmentos nao contem nenhum elemento.'), nl, nl,
	not(test_tsp3_remove_intersection(([copenhagen,belfast],[marseille,kiev]),[],_)),
	write('Test tsp3_remove_intersection passed!'), nl, nl,
	Res = true, !.

test_tsp3_remove_intersection(false).

test_convert_edges_to_circuito(EdgeList,ExpectedCircuito):-
	convert_edges_to_circuito(EdgeList,ExpectedCircuito).
	
test_convert_edges_to_circuito(Res):-
	write('Edges [belgrade,andorra] and [andorra,sofia] vao formar o circuito [belgrade,andorra,sofia].'), nl,
	test_convert_edges_to_circuito([[belgrade,andorra],[andorra,sofia]],[belgrade,andorra,sofia]),
	write('Edges [sarajevo,sarajevo] formam o circuito [sarajevo,sarajevo].'), nl,
	test_convert_edges_to_circuito([[sarajevo,sarajevo]],[sarajevo,sarajevo]),
	write('Impossivel converter uma lista de edges vazia num circuito.'), nl, nl,
	not(test_convert_edges_to_circuito([],_)),
	write('Test convert_edges_to_circuito passed!'), nl, nl,
	Res = true, !.

test_convert_edges_to_circuito(false).

test_calc_distancia(Cam,ExpectedDist):-
	calc_distancia(Cam,ExpectedDist).
	
test_calc_distancia(Res):-
	write('Caminho [pristina,podgorica,sarajevo,pristina] totaliza uma distancia de 587949.'), nl,
	test_calc_distancia([pristina,podgorica,sarajevo,pristina],587949),
	write('Circuito [madrid,madrid] tem uma distancia de 0.'), nl,
	test_calc_distancia([madrid,madrid],0),
	write('Impossivel calcular a distancia de um circuito sem elementos.'), nl, nl,
	not(test_calc_distancia([],_)),
	write('Test calc_distancia passed!'), nl, nl,
	Res = true, !.

test_calc_distancia(false).

test_tsp3(Orig,ExpectedCircuito,ExpectedDistTotal):-
	tsp3(Orig,Circuito,ExpectedDistTotal),
	length(Circuito,ExpectedCircuito).
	
test_tsp3(Res):-
	write('Circuito a partir de pristina percorrendo todas as cidades tem 52 elementos e distancia 24350415.'), nl,
	test_tsp3(pristina,52,24350415),
	write('kinshasa nao existe na lista de cidades.'), nl,
	not(test_tsp3(kinshasa,_,_)),
	write('Impossivel calcular o circuito sem fornecer uma cidade.'), nl, nl,
	not(test_tsp3(_,_,_)),
	write('Test tsp3 passed!'), nl, nl, nl,
	Res = true, !.

test_tsp3(false).

run_tests(Res):-
    test_outras_cidades_com_exclusoes(),
	test_tsp2_vizinho_mais_proximo(),
	test_tsp2(),
	test_convert_circuito_to_edges(),
	test_tsp3_find_first_intersect(),
	test_tsp3_remove_intersection(),
	test_convert_edges_to_circuito(),
	test_calc_distancia(),
	test_tsp3(),
    write('All tests passed!'),
	Res = true, !.
	
run_tests(false).
